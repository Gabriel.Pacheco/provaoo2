package Model;

public class Medico extends Usuario {
	private double salario;
	private double horasPorDia;
	private String area;
	private String hospital;
	private String estado;
	
	
	public void setSalario(double umSalario){
		this.salario = umSalario;
	}
	public double getSalario (){
		return salario;	
	}

	public void setHorasPorDia(double umaHora){
		this.HorasPorDia = umaHora;
	}
	public double getHorasPorDia (){
		return HorasPorDia;	
	}

	public void setArea (String umaArea){
		this.area = umaArea;	
	}

	public String getArea (){
		return area;	
	}
	
	public void setHospital (String umHospital){
		this.hospital = umHospital;	
	}

	public String getArea (){
		return hospital;	
	}
	
	public void setArea (String umEstado){
		this.estado = umEstado;	
	}

	public String getArea (){
		return estado;	
	}
}
