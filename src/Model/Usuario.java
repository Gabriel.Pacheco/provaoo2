package Model;

public class Usuario {
	private String nome;
	private String cpf;
	private String cep;
	
	
	public Usuario (String umNome){
		this.nome = umNome;
	}

	public String getNome (){
		return nome;	
	}

	public void setCpf (String umCpf){
		this.cpf = umCpf;	
	}

	public String getCpf (){
		return cpf;		
	}
	
	public void setCep (String umCep){
		this.cep = umCep;	
	}

	public String getCep (){
		return cep;		
	}
		
	

}
